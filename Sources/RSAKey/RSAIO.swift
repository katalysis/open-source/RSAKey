//
//  RSAIO.swift
//  BunqAPI
//
//  Created by Alex Tran-Qui on 27/03/2017.
//
//  Copy of Zewo/OpenSSL/IO.swift (https://github.com/Zewo/OpenSSL)
//  Zewo is distributed under the MIT license (https://github.com/Zewo/OpenSSL/blob/master/LICENSE)


import CNIOOpenSSL
import Foundation
//import Axis

public enum SSLIOError: Error {
    case io(description: String)
    case shouldRetry(description: String)
    case unsupportedMethod(description: String)
}

public class RSAIO {
    public enum Method {
        case memory
        
        var method: UnsafeMutablePointer<BIO_METHOD> {
            switch self {
            case .memory:
                return BIO_s_mem()
            }
        }
    }
    
    var bio: UnsafeMutablePointer<BIO>?
    
    public init(method: Method = .memory) throws {
        initialize()
        bio = BIO_new(method.method)
        
        if bio == nil {
            throw SSLIOError.io(description: RSAErrorDescription)
        }
    }
    
    public convenience init(buffer: Data) throws {
        try self.init()
        let numItems = buffer.count/MemoryLayout<UInt8>.stride

        _ = try buffer.withUnsafeBytes({ (data: UnsafePointer<UInt8>) -> Void in
            _ = try data.withMemoryRebound(to: UInt8.self, capacity: numItems) {
                try write(UnsafeBufferPointer(start: $0, count: numItems))
            }
        })
 
        
    }
    
    // TODO: crash???
    	deinit {
    		BIO_free(bio)
    	}
    
    public var pending: Int {
        return BIO_ctrl_pending(bio)
    }
    
    public var shouldRetry: Bool {
        return (bio!.pointee.flags & BIO_FLAGS_SHOULD_RETRY) != 0
    }
    
    // Make this all or nothing
    public func write(_ buffer: UnsafeBufferPointer<UInt8>) throws -> Int {
        guard !buffer.isEmpty else {
            return 0
        }
        
        let bytesWritten = BIO_write(bio, buffer.baseAddress!, Int32(buffer.count))
        
        guard bytesWritten >= 0 else {
            if shouldRetry {
                throw SSLIOError.shouldRetry(description: RSAErrorDescription)
            } else {
                throw SSLIOError.io(description: RSAErrorDescription)
            }
        }
        
        return Int(bytesWritten)
    }
    
    public func read(into: UnsafeMutableBufferPointer<UInt8>) throws -> Int {
        guard !into.isEmpty else {
            return 0
        }
        
        let bytesRead = BIO_read(bio, into.baseAddress!, Int32(into.count))
        
        guard bytesRead >= 0 else {
            if shouldRetry {
                throw SSLIOError.shouldRetry(description: RSAErrorDescription)
            } else {
                throw SSLIOError.io(description: RSAErrorDescription)
            }
        }
        
        return Int(bytesRead)
    }
}

