//
//  RSA.swift
//  RSAKey
//
//  Created by Giacomo Leopizzi on 27/04/2017.
//
//

import CNIOOpenSSL
//import Axis
import Foundation

final public class RSA {
    
    static public func sign(message msg: Data, key: RSAKey, withFunction function: RSAKey.Function = .sha256) throws -> Data {
        
        let ctx = EVP_MD_CTX_create()
        guard ctx != nil else {
            throw RSAKeyError.error(description: RSAErrorDescription)
        }
        
        return msg.withUnsafeBytes({ (messageBytes: UnsafePointer<UInt8>) -> Data in
                EVP_DigestInit_ex(ctx, function.evp, nil)
                EVP_DigestUpdate(ctx, UnsafeRawPointer(messageBytes), msg.count)
                var outLength: UInt32 = 0
                let ump = UnsafeMutablePointer<UInt8>.allocate(capacity: Int(EVP_PKEY_size(key.key)))
                EVP_SignFinal(ctx, ump, &outLength, key.key)
                return Data(bytes: ump, count: Int(outLength))
            })
    }
    
    static public func verify(message msg: Data, signature sig: Data, key: RSAKey, function: RSAKey.Function = .sha256) throws -> Bool {
        initialize()
        
        let ctx = EVP_MD_CTX_create()
        
        guard ctx != nil else {
            throw RSAKeyError.error(description: RSAErrorDescription)
        }
        
        return msg.withUnsafeBytes { (msgBytes: UnsafePointer<UInt8>) -> Bool in
            return sig.withUnsafeBytes { (sigBytes: UnsafePointer<UInt8>) -> Bool in
                EVP_DigestVerifyInit(ctx, nil, function.evp, nil, key.key)
                EVP_DigestUpdate(ctx, UnsafeRawPointer(msgBytes), msg.count)
                return (1 == EVP_DigestVerifyFinal(ctx, sigBytes, sig.count))
            }
        }
    }
    
    
    /// Encrypt a data object using EVP
    ///
    /// - Parameters:
    ///   - data: The data to encrypt
    ///   - key: The RSA for performing the encryption
    ///   - cipher: The cipher to use for the encryption
    ///   - padding: Padding for the RSA encryption (Optional)
    /// - Returns: The encrypted data
    /// - Throws: A RSAKeyError object
    static public func encrypt(data: NSData, withKey key: RSAKey, usingCipher cipher: RSA.Cipher, padding: RSA.Padding? = nil) throws -> EncryptedBundle {
        
        guard let context = EVP_CIPHER_CTX_new() else {
            throw RSAKeyError.error(description: RSAErrorDescription)
        }
        
        defer {
            EVP_CIPHER_CTX_free(context)
        }
        
        if let padding = padding {
            EVP_CIPHER_CTX_set_padding(context, padding.getPadding())
        }
        
        // ---> PREPARATION FOR EVP_SealInit
        
        // Preparation for the simmetric key
        let sizeSimmetricKey = EVP_PKEY_size(key.key).toInt()
        guard let simmetricKeyBuffer = NSMutableData(length: sizeSimmetricKey) else {
            throw RSAKeyError.error(description: "Unable to allocate memory")
        }
        let capacitySimmetricKeyBuffer = sizeSimmetricKey / MemoryLayout<UInt8>.size
        var pointerSimmetricKey: UnsafeMutablePointer<UInt8>? = simmetricKeyBuffer.mutableBytes.bindMemory(to: UInt8.self, capacity: capacitySimmetricKeyBuffer)
        var lenghtSimmetricKey: Int32 = 0 // After the EVP_SealInit it will contain the lenght of the simmetric key
        
        // Preparation for the iv buffer
        let sizeIVBuffer = EVP_CIPHER_iv_length(cipher.getCipher()).toInt()
        guard let ivBuffer = NSMutableData(length: sizeIVBuffer) else {
            throw RSAKeyError.error(description: "Unable to allocate memory")
        }
        let capacityIVBuffer = sizeIVBuffer / MemoryLayout<UInt8>.size
        let pointerIVBuffer = ivBuffer.mutableBytes.bindMemory(to: UInt8.self, capacity: capacityIVBuffer)
        
        // Get reference to the asimmetric key
        var pointerAsimmetricKey: UnsafeMutablePointer<EVP_PKEY>? = key.key
        
        
        guard EVP_SealInit(context, cipher.getCipher(), &pointerSimmetricKey, &lenghtSimmetricKey, pointerIVBuffer, &pointerAsimmetricKey, 1) != 0 else {
            throw RSAKeyError.error(description: "An error occurred in EVP_SealInit")
        }
        
        // ---> PREPARATION FOR EVP_EncryptUpdate
        
        let plaintextBufferPointer = data.bytes.assumingMemoryBound(to: UInt8.self)
        let plaintextLenght = data.length
        let cipherBlockLenght = EVP_CIPHER_block_size(cipher.getCipher()).toInt()
        var writtenBytes: Int32 = 0 // After the EVP_EncryptUpdate it will contain the number of bytes written to outBuffer
        
        let calculatedOutputLenght = plaintextLenght + cipherBlockLenght - (plaintextLenght % cipherBlockLenght)
        
        guard let outputBuffer = NSMutableData(length: calculatedOutputLenght) else {
            throw RSAKeyError.error(description: "Unable to allocate memory")
        }
        let outputBufferCapacity = calculatedOutputLenght / MemoryLayout<UInt8>.size
        let pointerOutputBuffer = outputBuffer.mutableBytes.bindMemory(to: UInt8.self, capacity: outputBufferCapacity)
        
        guard EVP_EncryptUpdate(context, pointerOutputBuffer, &writtenBytes, plaintextBufferPointer, plaintextLenght.toCInt()) == 1 else {
            throw RSAKeyError.error(description: "An error occurred in EVP_EncryptUpdate")
        }
        
        // ---> PREPARATION FOR EVP_SealFinal
        
        var finalWrittenBytes: Int32 = 0
        
        guard EVP_SealFinal(context, pointerOutputBuffer.advanced(by: writtenBytes.toInt()), &finalWrittenBytes) == 1 else {
            throw RSAKeyError.error(description: "An error occurred in EVP_SealFinal")
        }
        

        return EncryptedBundle(encryptedPayload: outputBuffer.data, encryptedSimmetricKey: simmetricKeyBuffer.data, iv: ivBuffer.data)        
        
    }
    
    /// Encrypt a data object using EVP
    ///
    /// - Parameters:
    ///   - data: The data to encrypt
    ///   - key: The RSAKey for performing the encryption
    ///   - cipher: The cipher to use for the encryption
    ///   - padding: Padding for the RSA encryption (Optional)
    /// - Returns: The encrypted data
    /// - Throws: A RSAKeyError object
    static public func encrypt(data: Data, withKey key: RSAKey, usingCipher cipher: RSA.Cipher, padding: RSA.Padding? = nil) throws -> EncryptedBundle {
        return try encrypt(data: data as NSData, withKey: key, usingCipher: cipher, padding: padding)
    }
    
    /// Decrypt an EncryptedBundle obejct using EVP
    ///
    /// - Parameters:
    ///   - bundle: The bundle to decrypt
    ///   - key: The private key for the decryption
    ///   - cipher: The cipher used for the encryption
    ///   - padding: Padding for the RSA encryption (Optional)
    /// - Returns: The decrypted data
    /// - Throws: A RSAKeyError object
    static public func decrypt(bundle: EncryptedBundle, withKey key: RSAKey, usingCipher cipher: RSA.Cipher, padding: RSA.Padding? = nil) throws -> NSData {
        
        guard let context = EVP_CIPHER_CTX_new() else {
            throw RSAKeyError.error(description: RSAErrorDescription)
        }
        
        defer {
            EVP_CIPHER_CTX_free(context)
        }
        
        if let padding = padding {
            EVP_CIPHER_CTX_set_padding(context, padding.getPadding())
        }
        
        let simmetricKeyPointer = (bundle.encryptedSimmetricKey as NSData).bytes.assumingMemoryBound(to: UInt8.self)
        let ivPointer = (bundle.iv as NSData).bytes.assumingMemoryBound(to: UInt8.self)
        
        guard EVP_OpenInit(context, cipher.getCipher(), simmetricKeyPointer, bundle.encryptedSimmetricKey.count.toCInt(), ivPointer, key.key) != 0 else {
            throw RSAKeyError.error(description: "An error occurred in EVP_OpenInit")
        }
        
        let payloadPointer = (bundle.encryptedPayload as NSData).bytes.assumingMemoryBound(to: UInt8.self)
        
        guard let outputBuffer = NSMutableData(length: bundle.encryptedPayload.count) else {
            throw RSAKeyError.error(description: "Unable to allocate memory")
        }
        let outputBufferCapacity = bundle.encryptedPayload.count / MemoryLayout<UInt8>.size
        let outputBufferPointer = outputBuffer.mutableBytes.bindMemory(to: UInt8.self, capacity: outputBufferCapacity)
        
        var writtenBytes: Int32 = 0
        
        guard EVP_DecryptUpdate(context, outputBufferPointer, &writtenBytes, payloadPointer, bundle.encryptedPayload.count.toCInt()) == 1 else {
            throw RSAKeyError.error(description: "An error occurred in EVP_DecryptUpdate")
        }
        
        var finalWrittenBytes: Int32 = 0
        
        guard EVP_OpenFinal(context, outputBufferPointer.advanced(by: writtenBytes.toInt()), &finalWrittenBytes) == 1 else {
            throw RSAKeyError.error(description: "An error occurred in EVP_OpenFinal")
        }
        
        let finalLenght = writtenBytes.toInt() + finalWrittenBytes.toInt()
        
        return NSData(bytes: outputBuffer.bytes, length: finalLenght)
    }
}
