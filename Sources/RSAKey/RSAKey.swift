//
//  RSAKey.swift
//  RSAKey
//
//  Created by Alex Tran-Qui on 14/03/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//  Heavily insprired by Zewo/OpenSSL (https://github.com/Zewo/OpenSSL/)
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//


import CNIOOpenSSL
import NIOOpenSSL
//import Axis
import Foundation

private var initialized = false

public func initialize() {
    guard !initialized else { return }
    SSL_library_init()
    SSL_load_error_strings()
    ERR_load_crypto_strings()
    OPENSSL_config(nil)
}

var RSAErrorDescription: String {
    let error = ERR_get_error()
    if let string = ERR_reason_error_string(error) {
        return String(validatingUTF8: string) ?? "Unknown Error"
    } else {
        return "Unknown Error"
    }
}

public enum RSAKeyError: Error {
    case error(description: String)
}

public class RSAKey {
    public enum Function {
        case md5, sha1, sha224, sha256, sha384, sha512
    }
    
    var key: UnsafeMutablePointer<EVP_PKEY>
    
    init(key: UnsafeMutablePointer<EVP_PKEY>) {
        initialize()
        self.key = key
    }
    
    init(io: RSAIO) throws {
        initialize()
        guard let _key = PEM_read_bio_PrivateKey(io.bio, nil, nil, nil) else {
            throw RSAKeyError.error(description: RSAErrorDescription)
        }
        self.key = _key
        
    }
    
    public convenience init(pemPrivString: String) throws {
        try self.init(io: RSAIO(buffer: Data([UInt8](pemPrivString.utf8))))
    }
    
    public init(pemPubString: String) throws { // this doesn't create a full RSAKey, just the pub key.
        initialize()
        let io = try RSAIO(buffer: Data([UInt8](pemPubString.utf8)))
        guard let _key = PEM_read_bio_PUBKEY(io.bio, nil, nil, nil) else {
            throw RSAKeyError.error(description: RSAErrorDescription)
        }
        //self.key = EVP_PKEY_new()
        key = _key
    }
    
    
    deinit {
        EVP_PKEY_free(key)
    }
    
    public var pubKey: String? {
        get {
            do {
                var io = try RSAIO()
                
                if ( 1 == PEM_write_bio_PUBKEY(io.bio, self.key)) {
                    let rawBufferCapacity = io.pending
                    let rawBuffer = UnsafeMutablePointer<UInt8>.allocate(capacity: rawBufferCapacity)
                    defer {
                        rawBuffer.deallocate(capacity: rawBufferCapacity)
                    }
                    
                    _ = try io.read(into: UnsafeMutableBufferPointer(start: rawBuffer, count: rawBufferCapacity))
                    
                    let s = Data(buffer: UnsafeMutableBufferPointer(start: rawBuffer, count: rawBufferCapacity))
                    let aa = s.filter{u in return true}
                    return aa.reduce("") { (r, u) in return r + String(UnicodeScalar(u)) }
                }
            } catch let error {
                print("\(error)")
            }
            return nil
        }
    }
    
    public static func generate(keyLength: Int32 = 2048) -> RSAKey {
        let key = RSAKey(key: EVP_PKEY_new())
        let rsa = RSA_new()
        let exponent = BN_new()
        BN_set_word(exponent, 0x10001)
        RSA_generate_key_ex(rsa, keyLength, exponent, nil)
        EVP_PKEY_set1_RSA(key.key, rsa)
        return key
    }
    
}

extension RSAKey.Function {
    
    internal var evp: UnsafePointer<EVP_MD> {
        switch self {
        case .md5:
            return EVP_md5()
        case .sha1:
            return EVP_sha1()
        case .sha224:
            return EVP_sha224()
        case .sha256:
            return EVP_sha256()
        case .sha384:
            return EVP_sha384()
        case .sha512:
            return EVP_sha512()
        }
    }
}

