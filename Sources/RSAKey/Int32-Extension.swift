//
//  Int32-Extension.swift
//  RSAKey
//
//  Created by Giacomo Leopizzi on 27/04/2017.
//
//

import Foundation

internal extension Int32 {
    
    func toInt() -> Int {
        return Int(self)
    }
    
}
